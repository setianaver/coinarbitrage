const fs = require("fs");
const CJson = require("comment-json");

function loadConfig(config_name) {
    let filename = process.cwd() + "/config/" + config_name + ".json";
    if (fs.existsSync(filename)) {
        let contents = fs.readFileSync(filename);
        let config_item = CJson.parse(contents.toString());
        return config_item;
    }
}

exports.init = function (type) {
    return loadConfig(type);
};

