const mariadb = require('mariadb');

let pool;
let conn;

exports.connect = async function() {

    global.config
    if (!conn) {
        pool = mariadb.createPool({
            host: global.config.DATABASE.host, port: global.config.DATABASE.port,
            user: global.config.DATABASE.user, password: global.config.DATABASE.password,
            database: global.config.DATABASE.database,
            connectionLimit: 5
        });

        conn = await pool.getConnection();
    }
}

exports.get_account_list = async function(){
    let rows;
    try{
        rows = await conn.query('SELECT exch, market, avail_balance, lock_balance FROM accounts');
    }
    catch(err){
        throw err;
    }
    finally{
        return rows[0];
    }
}



exports.reset_exch_account = async function(param){
    let conn, rows;
    try{
        conn = await pool.getConnection();
        let sql = "UPDATE accounts SET avail_balance = 0, lock_balance = 0 WHERE exch=?"
        rows = await conn.query(sql, [param.exch]);
    }
    catch(err){
        throw err;
    }
    finally{
        return rows[0];
    }
}

exports.insert_account = async function(param){
    let rows;
    try{

        let sql = "INSERT INTO accounts";
        sql += " (exch, market, avail_balance, lock_balance) VALUES(?, ?, ?, ?)"
        sql += " ON DUPLICATE KEY UPDATE avail_balance=VALUES(avail_balance), lock_balance=VALUES(lock_balance)"
        rows = await conn.query(sql, [param.exch, param.market, param.avail, param.locked]);
    }
    catch(err){
        throw err;
    }
    finally{
        return true;
    }
}


exports.insert_order = async function(param){
    let rows;
    try{
        let sql = "INSERT INTO orders "
        sql += "(exch, market, price, volume, remain_volume, order_type, status, exch_order_id) VALUES(?,?,?,?,?,?,?)"
        rows = await conn.query(sql, [param.exch, param.market, param.price, param.volume,
                    param.remain_volume, param.order_type, param.status, param.exch_order_id]);
    }
    catch(err){
        throw err;
    }
    finally{
        return true;
    }
}