const axios = require('axios')
const request = require('request')
const {Bithumb} = require('bithumb');

Bithumb.setApiKey(global.config.BITHUMB.access_key, global.config.BITHUMB.secret_key);

var server_url = 'https://api.bithumb.com';
var coin_list = global.config.COIN_LIST


/*
시세 조회
IN

OUT
    [
        {"BTC": 57300000},
        {"ETH": 3100000},
    ]
*/
exports.ticker = async () => {
    var options = {
        method: 'GET',
        url: ""
    };

    options.url = server_url + '/public/ticker/all'
    var ret = await axios.get(options.url);

    var resp = {};
    for (let coin of coin_list) {
        resp[coin] = parseFloat(ret.data.data[coin].closing_price);
    }
    return resp;
}

/*
호가 조회(5호가)
IN
OUT
    {
     "BTC" :
           {
            "BUY" :
                [
                {price : "100", qty: "0.01"},
                {price : "100", qty: "0.01"}
                ],
            "SELL" :
                [
                {price : "100", qty: "0.01"},
                {price : "100", qty: "0.01"}
                ]
         }
*/
exports.hoga = async () => {
    var options = {
        method: 'GET',
        url: ""
    };

    options.url = server_url + '/public/orderbook/all'
    var ret = await axios.get(options.url);

    function _conv(arr) {
        var newitem = []
        for (var itm of arr) {
            newitem.push({price: itm.price, qty: itm.quantity});
        }
        return newitem;
    }

    var resp = {};
    for (let coin of coin_list) {
        resp[coin] = {
            BUY: _conv(ret.data.data[coin].bids),
            SELL: _conv(ret.data.data[coin].asks)
        };
    }
    return resp;
}


/*
잔고 조회
IN    
OUT
    [
        {"KRW": {balance: 23240000, locked: 111}},
        {"BTC": {balance: 23240000, locked: 111}},
        {"ETH": {balance: 23240000, locked: 111}}
    ]
*/
exports.get_accounts = async (callback) => {

    var ret = await Bithumb.getMyBalance();

    var data = ret[2].data;
    var resp = {};
    if ("total_krw" in data) {
        resp['KRW'] = {
            balance: parseFloat(data.available_krw),
            locked: parseFloat(data.total_krw) - parseFloat(data.available_krw)
        };
    }
    for (var item of coin_list) {
        var itemL = item.toLocaleLowerCase();
        if ("total_" + itemL in data) {
            var avail = parseFloat(data["available_" + itemL]);
            var total = parseFloat(data["total_" + itemL]);
            if (total > 0) {
                resp[item] = {balance: avail, locked: total - avail};
            }
        }
    }

    return resp;
}


/*
주문
IN    
  { 
    market: 'BTC',
    side: 'buy',
    volume: '0.01', // 시장가 매수시 금액, 매도시 수량
    price: '100',
    ord_type: 'limit',
    identifier: '1'
  }
OUT    
    성공시 : 주문번호
    실패시 : false
*/
exports.order = async (param) => {

    var type = param.side.toUpperCase() === 'BUY' ? 'bid' : 'ask';
    var currencyType = param.market.toUpperCase();
    var price = param.price;
    var count = param.volume;

    /*
    [성공시]
    ret = Array(3) [null, IncomingMessage, Object]
        0 = null
        1 = IncomingMessage {_readableState: ReadableState,
        2 = Object {status: "0000", order_id: "C0112000000200621313"}
        length = 3
     */
    var ret;
    if (param.ord_type === 'market') {
        if ( type === "bid")
            ret = await market_buy(currencyType, count)
        else
            ret = await market_sell(currencyType, count)

    } else {
        ret = await limit_order(type, currencyType, price, count)

    }
    if (ret[2].status === "0000")
        return ret[2].order_id

    return null
}

async function limit_order(type, currencyType, price, count) {
    const uri = '/trade/place';
    const data = {
        type,
        price,
        order_currency: currencyType,
        units: count,
        payment_currency: "KRW"
    };
    return await call(uri, data);
}

/**
 * Place a purchase order at a market price.
 * */
async function market_buy(currencyType, count) {
    const uri = '/trade/market_buy';
    const data = {
        order_currency: currencyType,
        payment_currency: "KRW",
        units: count
    };
    return await call(uri, data);
}

/**
 * Place a sales order at a market price.
 * */
async function market_sell(currencyType, count) {
    const uri = '/trade/market_sell';
    const data = {
        order_currency: currencyType,
        payment_currency: "KRW",
        units: count
    };
    return await call(uri, data);
}


/*
주문체결여부 조회
IN
  {
    market: 'BTC',
    side: 'buy',
    order_id: '123232301',
  }
OUT
    성공시 : true
    실패시 : false
*/
exports.order_complete_test = async (param) => {

    var side = param.side.toLowerCase() === "buy" ? 'bid' : 'ask'
    var currencyType = param.market
    var orderId = param.order_id

    var ret = await getIncompleteOrders(side, currencyType, orderId)
    if (ret[2].status === "0000") {
        return ret[2].data[0].units_remaining === "0"
    }
    return false
}

async function getIncompleteOrders(side, currencyType, orderId,) {
    const uri = '/info/orders';
    const data = {
        type: side,
        order_id: orderId,
        order_currency: currencyType
    };
    return await call(uri, data);
}


function call(uri, data) {
    return new Promise((resolve) => {
        request.post({
            url: `${Bithumb._apiUrl}${uri}`,
            json: true,
            headers: {
                ...Bithumb.makeAuthenticationHeaders(uri, data)
            },
            form: data
        }, (err, httpRes, body) => {
            resolve([err, httpRes, body]);
        });
    });
}
