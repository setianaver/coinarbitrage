var CoinoneAPI = require('./lib/coinone');


const access_key = global.config.UPBIT.access_key
const secret_key = global.config.UPBIT.secret_key
const coinoneAPI = new CoinoneAPI(access_key, secret_key)


var coin_list = global.config.COIN_LIST

/*
시세 조회
IN
    ["BTC", "ETH"]
OUT
    [
        {"BTC": 57300000},
        {"ETH": 3100000},
    ]
*/
exports.ticker = async () => {

    var ret = await coinoneAPI.ticker('all');

    var resp = {};
    for (let coin of coin_list) {
        var key = coin.toLowerCase();
        if (key in ret.data) {
            resp[coin] = parseFloat(ret.data[key].last);
        }
    }

    return resp;
}


/*
호가 조회(5호가)
IN
OUT
    {
     "BTC" :
           {
            "BUY" :
                [
                {price : "100", qty: "0.01"},
                {price : "100", qty: "0.01"}
                ],
            "SELL" :
                [
                {price : "100", qty: "0.01"},
                {price : "100", qty: "0.01"}
                ]
         }
*/
exports.hoga = async () => {
    var resp = {};
    for (let coin of coin_list) {
        var ret = await coinoneAPI.orderbook(coin.toLowerCase());
        resp[coin] = {BUY: ret.data.bid.slice(0, 5), SELL: ret.data.ask.slice(0, 5)};
    }
    return resp;
}

/*
잔고 조회
IN    
OUT
    [
        {"KRW": {balance: 23240000, locked: 111}},
        {"BTC": {balance: 23240000, locked: 111}},
        {"ETH": {balance: 23240000, locked: 111}}
    ]
*/
exports.get_accounts = async (callback) => {

 //   var ret = coinoneAPI.balance();
    return [{KRW: 111}];

    var resp = [];

    for (let coin of coin_list) {
        var key = coin.toLowerCase();
        if (key in ret.data) {
            var dd = {};
            dd[coin] = parseFloat(ret.data[key].avail);
            resp.push(dd);
        }
    }

    return resp;
}


/*
주문
IN    
  { 
    market: 'BTC',
    side: 'buy',
    volume: '0.01', // 시장가 매수시 금액, 매도시 수량
    price: '100',
    ord_type: 'limit',
    identifier: '1'
  }
OUT    
   성공시 : 주문번호
   실패시 : false
   
[주의!] 시장가가 없다
*/
exports.order = async (param) => {
    var ret;
    if (param.side.toUpperCase() === "BUY") {
        ret = coinoneAPI.limitBuy(param.market.toLowerCase(), param.price, param.volume);
    } else if (param.side.toUpperCase() === "SELL") {
        ret = coinoneAPI.limitSell(param.market.toLowerCase(), param.price, param.volume);
    } else {
        return false;
    }
    return ret.orderId;
}


/*
주문체결여부 조회
IN
  {
    market: 'BTC',
    side: 'buy',
    order_id: '123232301',
  }
OUT
    성공시 : true
    실패시 : false
*/
exports.order_complete_test = async (param) => {
    var ret = coinoneAPI.infoOrder(param.market.toUpperCase(), param.order_id)
    return ret.info.remainQty === "0.0"
}