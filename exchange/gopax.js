const axios = require('axios')
const XMLHttpRequest = require('xmlhttprequest').XMLHttpRequest;
const crypto = require('crypto');

const access_key = global.config.GOPAX.access_key
const secret_key = global.config.GOPAX.secret_key
const server_url = "https://api.gopax.co.kr"

// 2021-05-31 : Gopax에는 ADA, DOT가 없음
var coin_list = global.config.COIN_LIST

const API_KEY = access_key;
const SECRET = secret_key;

function call(needAuth, method, path, bodyJson = null, recvWindow = null) {
    const bodyStr = bodyJson ? JSON.stringify(bodyJson) : '';
    const http = new XMLHttpRequest();
    const methodInUpperCase = method.toUpperCase();
    http.open(methodInUpperCase, `https://api.gopax.co.kr${path}`, false);
    if (needAuth) {
        const timestamp = new Date().getTime();
        const includeQuerystring = methodInUpperCase === 'GET' && path.startsWith('/orders?');
        const p = includeQuerystring ? path : path.split('?')[0];
        const msg = `t${timestamp}${methodInUpperCase}${p}${recvWindow || ''}${bodyStr}`;
        const rawSecret = Buffer.from(SECRET, 'base64');
        const signature = crypto.createHmac('sha512', rawSecret).update(msg).digest('base64');
        http.setRequestHeader('api-key', API_KEY);
        http.setRequestHeader('timestamp', timestamp);
        http.setRequestHeader('signature', signature);
        if (recvWindow) {
            http.setRequestHeader('receive-window', recvWindow);
        }
    }
    http.send(bodyStr);
    return {
        statusCode: http.status,
        body: JSON.parse(http.responseText),
        header: http.getAllResponseHeaders(),
    };
}

/*
시세 조회
IN
    ["BTC", "ETH"]
OUT
    [
        {"BTC": 57300000},
        {"ETH": 3100000},
    ]
*/
exports.ticker = async () => {

    const options = {
        method: 'GET',
        url: server_url + "/trading-pairs/stats"
    };

    var ret = await axios.get(options.url);
    var resp = {};

    for (let ticker of ret.data) {
        var ticker_name = ticker.name.replace("-KRW", "");
        if (coin_list.indexOf(ticker_name) > -1) {
            resp[ticker_name] = parseFloat(ticker.close);
        }
    }

    return resp;
}


/*
호가 조회(5호가)
IN
OUT
    {
     "BTC" :
           {
            "BUY" :
                [
                {price : "100", qty: "0.01"},
                {price : "100", qty: "0.01"}
                ],
            "SELL" :
                [
                {price : "100", qty: "0.01"},
                {price : "100", qty: "0.01"}
                ]
         }
*/
exports.hoga = async () => {
    const options = {
        method: 'GET',
        url: server_url + "/trading-pairs/"
    };

    function _conv(arr) {
        var newitem = []
        for (var itm of arr) {
            newitem.push({price: itm[1], qty: itm[2]});
        }
        return newitem;
    }


    var resp = {};
    for (var coin of coin_list) {
        try {
            var ret = await axios.get(options.url + coin + '-KRW/book?level=2');

            resp[coin] = {BUY: _conv(ret.data.bid.slice(0, 5)), SELL: _conv(ret.data.ask.slice(0, 5))};
        } catch (e) {
            resp[coin] = null;
        }
    }

    return resp;
}

/*
잔고 조회
IN    
OUT
    [
        {"KRW": {balance: 23240000, locked: 111}},
        {"BTC": {balance: 23240000, locked: 111}},
        {"ETH": {balance: 23240000, locked: 111}}
    ]
*/
exports.get_accounts = async (callback) => {

    var ret = call(true, 'GET', '/balances');

    var balances = ret.body;
    var resp = {};
    for (var item of balances) {
        if (parseFloat(item.avail) > 0 || parseFloat(item.hold) > 0) {
            resp[item.asset] = {balance: parseFloat(item.avail), locked: parseFloat(item.hold)};
        }
    }

    return resp;
}


/*
주문
IN    
  { 
    market: 'BTC',
    side: 'buy',
    volume: '0.01', // 시장가 매수시 금액, 매도시 수량
    price: '100',
    ord_type: 'limit',
    identifier: '1'
  }
OUT    
   성공시 : 주문번호
   실패시 : false
*/

/*
  - 시장가 최소 주문금액 10,000원
  - ETH-KRW 오더북에서 지정가는 매도 매수 관계없이 단위가 ETH인 반면,
    시장가 매도는 단위가 ETH이고 시장가 매수는 단위가 KRW이 됩니다.
 */

exports.order = async (param) => {
    let postOrdersReqBody = {
        tradingPairName: param.market + '-KRW',
        side: param.side,
        type: param.ord_type,
        price: param.price,
        amount: param.volume
    };

    if (param.ord_type === 'market') {
        delete postOrdersReqBody.price
    }


    var ret = call(true, 'POST', '/orders', postOrdersReqBody, 200);
    return ret.body.id;
}


/*
주문체결여부 조회
IN
  {
    market: 'BTC',
    side: 'buy',
    order_id: '123232301',
  }
OUT
    성공시 : true
    실패시 : false
*/
exports.order_complete_test = async (param) => {

    var ret = call(true, 'GET', '/orders/' + param.order_id);
    return ret.body.remaining == "0";
}