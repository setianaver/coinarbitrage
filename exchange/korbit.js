
const crypto = require('crypto');

const access_key = global.config.KORBIT.access_key
const secret_key = global.config.KORBIT.secret_key

// 2021-05-31 : Korbit에는 DOGE가 없음
var coin_list = global.config.COIN_LIST
const KORBIT = require('./lib/korbit')

var korbit = new KORBIT(access_key, secret_key, "", "");


/*
시세 조회
IN
    ["BTC", "ETH"]
OUT
    [
        {"BTC": 57300000},
        {"ETH": 3100000},
    ]
*/
exports.ticker = async () => {
    var ret = await korbit.ticker(true);
    var resp = {};

    for (let coin of coin_list) {
        var key = coin.toLowerCase() + "_krw";
        if (key in ret) {
            resp[coin] = parseFloat(ret[key].last);
        }
    }
    return resp;
}


/*
호가 조회(5호가)
IN
OUT
    {
     "BTC" :
           {
            "BUY" :
                [
                {price : "100", qty: "0.01"},
                {price : "100", qty: "0.01"}
                ],
            "SELL" :
                [
                {price : "100", qty: "0.01"},
                {price : "100", qty: "0.01"}
                ]
         }
*/
exports.hoga = async () => {

    function _conv(arr) {
        var newitem = []
        for (var itm of arr) {
            newitem.push({price: itm[0], qty: itm[1]});
        }
        return newitem;
    }

    var resp = {};
    for (var coin of coin_list) {
        try {
            var ret = await korbit.orderbook(coin);

            resp[coin] = {BUY: _conv(ret.bids.slice(0, 5)), SELL: _conv(ret.asks.slice(0, 5))};
        } catch (e) {
            resp[coin] = null;
        }
    }

    return resp;
}


/*
잔고 조회
IN    
OUT
    [
        {"KRW": {balance: 23240000, locked: 111}},
        {"BTC": {balance: 23240000, locked: 111}},
        {"ETH": {balance: 23240000, locked: 111}}
    ]
*/
exports.get_accounts = async (callback) => {

    var balances = await korbit.balances()

    var resp = {};
    for (var key in balances) {
        var item = balances[key];
        if (parseFloat(item.available) <= 0
            && parseFloat(item.trade_in_use) <= 0
            && parseFloat(item.withdrawal_in_use) <= 0)
            continue;

        resp[key.toUpperCase()] = {
            balance: parseFloat(item.available),
            locked: parseFloat(item.trade_in_use) + parseFloat(item.withdrawal_in_use)
        };

    }
    return resp;
}


/*
주문
IN    
  { 
    market: 'BTC',
    side: 'buy',
    volume: '0.01', // 시장가 매수시 금액, 매도시 수량
    price: '100',
    ord_type: 'limit',
    identifier: '1'
  }
OUT    
   성공시 : 주문번호
   실패시 : false

   시장가 주문(type=market)일 경우 coin_amount와 fiat_amount중 하나만 설정해야 하며
   (둘 다 설정할 경우 HTTP Status Code 400 반환),
   coin_amount를 설정하는 경우 지정한 수량만큼 시장가로 매수한다.

   fiat_amount : 시장가 주문(type=market)일 때만 유효한 파라미터이며
   , coin_amount와 같이 사용할 수 없다.(둘 다 설정할 경우 HTTP Status Code 400 반환)
*/
exports.order = async (param) => {

    var body = {
        currency_pair: param.market.toLowerCase() + '_krw',
        price: param.price,
        type: param.ord_type,
    }

    // 시장가일때는 volume이 KRW 단위
    if (param.ord_type.toLowerCase() === 'limit') {
        body["coin_amount"] = param.volume
    } else {
        body["fiat_amount"] = param.volume
    }

    var ret;
    if (param.side.toUpperCase() === "BUY")
        ret = await korbit.bidOrder(body)
    else
        ret =  await korbit.askOrder(body)
    ret = JSON.parse(ret)
    return ret.orderId;
}


/*
주문체결여부 조회
IN
  {
    market: 'BTC',
    side: 'buy',
    order_id: '123232301',
  }
OUT
    성공시 : true
    실패시 : false
*/
exports.order_complete_test = async (param) => {

    var param = {
        currency_pair: param.market.toLowerCase() + "_krw",
        id: param.order_id
    }
    var ret = await korbit.orders(param)
    return ret[0].status === "filled";
}