'use strict';

var needle = require('needle');
var _ = require('lodash');

/**
 * Korbit API wrapper
 * @param {String} clientID
 * @param {String} clientSecret
 * @param {String} userName
 * @param {String} userPassword
 **/

class Korbit {

    constructor(clientID, clientSecret, userName, userPassword){
        if(_.isEmpty(clientID) || _.isEmpty(clientSecret)){
            throw new Error("Missing Parameters");
        }

        this.config = {
            url : 'https://api.korbit.co.kr/',
            version : 'v1',
            timeoutMS : 18000
        };

        this.clientID = clientID;
        this.clientSecret = clientSecret;
        this.userName = userName;
        this.userPassword = userPassword;
    }

    /*
     * PUBLIC METHOD
     */

    ticker (isDetailed){
        return new Promise((resolve, reject) => {
            var func = (aa,ret,bb) => {
                resolve(ret.body);
            };

            if (isDetailed == true)
                return this.requestPublicAPI('/ticker/detailed/all', func);
            else
                return this.requestPublicAPI('/ticker', func);
        });
    }

    async balances(){

        return await this.requestPrivateAPI('GET', '/user/balances');

    }


    orderbook (coin){
        var parameters = {
            currency_pair : coin.toLowerCase() + '_krw'
        };
        return new Promise((resolve, reject) => {
            var func = (aa,ret,bb) => {
                resolve(ret);
            };
             this.requestPublicAPI('/orderbook', parameters, func);
        });
    }

    transactions (params, callback){
        var parameters = {
            time : params.time ? params.time : 'hour'
        };

        return this.requestPublicAPI('/transactions', parameters, callback);
    }

    constants (callback){
        return this.requestPublicAPI('/constants', callback);
    }

    /*
     * PRIVATE METHOD
     */

    /* AUTH METHOD */
    async authorize() {

        return this.requestAuthAPI('/oauth2/access_token');
    }

    refreshAccessToken(callback) {
        return this.requestAuthAPI('/oauth2/access_token', callback);
    }

    /* USER METHOD */
    getUserInfo (callback) {
        return this.requestPrivateAPI('GET', '/user/info', callback);
    }

    async bidOrder (params){
        return await this.requestPrivateAPI('POST', '/user/orders/buy', params);
    }

    async askOrder (params){
        return await this.requestPrivateAPI('POST', '/user/orders/sell', params);
    }

    async orders (params){
        return await this.requestPrivateAPI('GET', '/user/orders', params);
    }

    cancelOrder(params, callback){
        return this.requestPrivateAPI('POST', '/user/orders/cancel', params, callback);
    }

    listOrders(callback){
        return this.requestPrivateAPI('GET', '/user/orders/open', callback);
    }

    transactionHistory(params, callback){
        return this.requestPrivateAPI('GET', '/user/transaction', callback);
    }


    /* FIAT METHOD */

    setVirtualBank(params, callback){
        return this.requestPrivateAPI('POST', '/user/fiats/address/assign', callback);
    }

    setBankAccount(params, callback){
        return this.requestPrivateAPI('POST', '/user/fiats/address/register', params, callback);
    }

    requestWithdrawal(params, callback){
        return this.requestPrivateAPI('POST', '/user/fiats/out', params, callback);
    }

    withdrawalStatus(callback){
        return this.requestPrivateAPI('GET', '/user/fiats/status', callback);
    }

    cancelWithdrawal(params, callback){
        return this.requestPrivateAPI('POST', '/user/fiats/out/cancel', params, callback);
    }


    /* WALLET METHOD */

    getWalletStatus(callback){
        return this.requestPrivateAPI('GET', '/user/wallet', callback);
    }

    assignWalletAddr(params, callback){
        return this.requestPrivateAPI('POST', '/user/conins/address/assign', params, callback);
    }

    requestBTCWithdrawal(params, callback){
        return this.requestPrivateAPI('POST', '/user/coins/out', params, callback);
    }

    btcWithdrawalStatus(callback){
        return this.requestPrivateAPI('GET', '/user/coins/status', callback);
    }

    cancelBTCWithdrawal(params, callback){
        return this.requestPrivateAPI('POST', '/user/coins/out/cancel', callback);
    }

    /*
     * UTILITY METHOD
     */

    generateNonce(){
        return new Date().getTime();
    }

    /*
     * Request method for public API
     */

    requestPublicAPI (path, params, callback){
        if(callback === undefined){
            callback = params;
        }

        needle.get(this.config.url + this.config.version + path, params, function(err, response){
            if(!err && response.statusCode == 200){
                return callback(null, response.body);
            } else {
                return callback(new Error(response.headers.warning), null);
            }
        });
    }

    /*
     * Request method for private API (with access token)
     */

    queryString( obj ) {
        if (!obj)
            return '';

        let str = Object.keys(obj).reduce(function(a, k){
            a.push(k + '=' + encodeURIComponent(obj[k]));
            return a;
        }, []).join('&');
        return str;
    }
    async requestPrivateAPI (method, path, params) {

        let ret = await this.authorize();

        if (_.isEmpty(this.clientID) || _.isEmpty(this.clientSecret)) {
            return "Credentials are not set.";
        }

        var options = {
            headers: {
                'Accept': 'application/json',
                'Authorization': _.isEmpty(this.accessToken) ? null : 'Bearer ' + this.accessToken
            }
        };

        return new Promise((resolve, reject) => {
            if (method == 'GET') {
                needle.get(this.config.url + this.config.version + path + '?nonce=' + this.generateNonce() + "&" + this.queryString(params), options, function (err, response) {
                    if (!err && response.statusCode == 200) {
                        resolve(response.body);
                    } else {
                        resolve(response.headers.warning);
                    }
                });
            } else {
                params.nonce = this.generateNonce();
                needle.post(this.config.url + this.config.version + path, params, options, function (err, response) {
                    if (!err && response.statusCode == 200 && response.statusMessage == 'OK') {
                        resolve(response.body);
                    } else if (response.statusCode == 200 && response.statusMessage != 'OK') {
                        resolve(response.body.status);
                    } else {
                        resolve(response.headers.warning);
                    }
                });
            }
        });
    }


    /*
     * Request method for authentication APIs
     */

    async requestAuthAPI (path){
        var self = this;
        return new Promise((resolve, reject) => {
            if(_.isEmpty(this.clientID) || _.isEmpty(this.clientSecret)){
                reject("Credentials are not set.");
            } else {

                // access키 만료전에는 그냥 리턴
                if (self.expires > Date.now()) {
                    resolve("");
                    return;
                }

                var body = {
                    client_id: this.clientID,
                    client_secret: this.clientSecret,
                    grant_type: _.isEmpty(this.refreshToken) ? 'client_credentials' : 'refresh_token'
                };

                if (!_.isEmpty(this.refreshToken))
                    body.refresh_token = this.refreshToken;

                needle.post(this.config.url + this.config.version + path, body, function (err, response) {
                    if (!err && response.statusCode == 200) {
                        self.refreshToken = response.body['refresh_token'];
                        self.accessToken = response.body['access_token'];
                        self.expires = new Date().getTime() + response.body['expires_in'];
                        return resolve(response.body);
                    } else {
                        return reject("Failed to authorize. " + response.headers.warning);
                    }
                });
            }

        });
    }
}

module.exports = Korbit;