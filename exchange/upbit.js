const axios = require('axios')
const {v4: uuidv4} = require('uuid');
const request = require('request')
const crypto = require('crypto');
const sign = require('jsonwebtoken').sign

const queryEncode = require("querystring").encode

const access_key = global.config.UPBIT.access_key
const secret_key = global.config.UPBIT.secret_key
const server_url = "https://api.upbit.com"

var coin_list = global.config.COIN_LIST

/*
시세 조회
IN
    ["BTC", "ETH"]
OUT
    [
        {"BTC": 57300000},
        {"ETH": 3100000},
    ]
*/
exports.ticker = async () => {

    var list = "KRW-" + coin_list.join(",KRW-");
    const options = {
        method: 'GET',
        url: server_url + "/v1/ticker?markets=" + list
    };

    var ret = await axios.get(options.url);
    var resp = {};
    for (var item of ret.data) {
        resp[item.market.replace('KRW-', '')] = parseFloat(item.trade_price);
    }
    return resp;
}


/*
호가 조회(5호가)
IN
OUT
    {
     "BTC" :
           {
            "BUY" :
                [
                {price : "100", qty: "0.01"},
                {price : "100", qty: "0.01"}
                ],
            "SELL" :
                [
                {price : "100", qty: "0.01"},
                {price : "100", qty: "0.01"}
                ]
         }
*/
exports.hoga = async () => {
    var list = "KRW-" + coin_list.join(",KRW-");
    const options = {
        method: 'GET',
        url: server_url + "/v1/orderbook?markets=" + list
    };

    function _conv(arr) {
        arr = arr.orderbook_units.slice(0, 5)

        var buyitem = []
        var sellitem = []
        for (var itm of arr) {
            buyitem.push({price: itm.bid_price, qty: itm.bid_size});
            sellitem.push({price: itm.ask_price, qty: itm.ask_size});
        }
        return {BUY: buyitem, SELL: sellitem};
    }

    var ret = await axios.get(options.url);
    var resp = {};
    for (var item of ret.data) {
        resp[item.market.replace('KRW-', '')] = _conv(item)
    }

    return resp;
}


/*
잔고 조회
IN    
OUT
    [
        {"KRW": {balance: 23240000, locked: 111}},
        {"BTC": {balance: 23240000, locked: 111}},
        {"ETH": {balance: 23240000, locked: 111}}
    ]
*/
exports.get_accounts = async (callback) => {

    const payload = {
        access_key: access_key,
        nonce: uuidv4(),
    }

    const token = sign(payload, secret_key);
    const options = {
        method: "GET",
        url: server_url + "/v1/accounts",
        headers: {Authorization: `Bearer ${token}`},
    };

    var ret = await axios.request(options);

    var resp = {};
    for (var item of ret.data) {
        resp[item.currency] = {balance: parseFloat(item.balance), locked: parseFloat(item.locked)};
    }
    return resp;
}


/*
주문
IN    
  { 
    market: 'BTC',
    side: 'buy',
    volume: '0.01', // 시장가 매수시 금액, 매도시 수량
    price: '100',
    ord_type: 'limit',
    identifier: '1'
  }
OUT    
    "ok"
    "error"
*/
exports.order = async (param) => {

    param.market = 'KRW-' + param.market.toUpperCase();
    if (param.side.toLowerCase() === 'buy')
        param.side = 'bid';

    if (param.side.toLowerCase() === 'sell')
        param.side = 'ask';

    if (param.ord_type.toLowerCase() === "market") {
        if (param.side.toLowerCase() === 'buy') {
            param.ord_type = "price"
            param.price = param.volume
        }
    }

    const query = queryEncode(param)
    const hash = crypto.createHash('sha512')
    const queryHash = hash.update(query, 'utf-8').digest('hex')
    const payload = {
        access_key: access_key,
        nonce: uuidv4(),
        query_hash: queryHash,
        query_hash_alg: 'SHA512',
    }

    const token = sign(payload, secret_key)
    const options = {
        method: "POST",
        url: server_url + "/v1/orders",
        headers: {Authorization: `Bearer ${token}`},
        json: param
    }

    return new Promise((resolve, reject) => {
        request(options, (error, response, body) => {
            if (error)
                return reject(error)
            resolve(body.uuid)
        })
    });
}

/*
주문체결여부 조회
IN
  {
    market: 'BTC',
    side: 'buy',
    order_id: '123232301',
  }
OUT
    성공시 : true
    실패시 : false
*/
exports.order_complete_test = async (param) => {

    const body = {
        uuid: param.order_id
    }
    const query = queryEncode(body)
    const hash = crypto.createHash('sha512')
    const queryHash = hash.update(query, 'utf-8').digest('hex')
    const payload = {
        access_key: access_key,
        nonce: uuidv4(),
        query_hash: queryHash,
        query_hash_alg: 'SHA512'
    }

    const token = sign(payload, secret_key);
    const options = {
        method: "GET",
        url: server_url + "/v1/order?" + query ,
        headers: {Authorization: `Bearer ${token}`},
        json: body
    };

    var ret = await axios.request(options);
    return ret.data.remaining_volume === "0.0";
}