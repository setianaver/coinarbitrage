const { createLogger, format, transports } = require('winston');
const { combine, timestamp, label, printf } = format

const util = require('util')
const fs = require('fs');
var path = require('path');
const logDir = path.join(__dirname, 'log');
let logger = null;

function init(name) {
    if (!fs.existsSync(logDir)) {
        fs.mkdirSync(logDir);
    }


    const myFormat = printf(({ level, message, label, timestamp }) => {
        return `${timestamp} ${level}: ${util.inspect(message, {showHidden: false, depth: null}).replace(/\r?\n/g, '')}`
    })

    logger = createLogger({
        format: combine(label({ label: '' }),  format.timestamp({
            format: 'YYYY-MM-DD HH:mm:ss'
        }), myFormat),
        transports: [new transports.Console(),
                new (require('winston-daily-rotate-file'))({
                    filename: `${logDir}/${name}-%DATE%.log`,

                    datePattern: 'YYYYMMDD',
                    prepend: true,
                    zippedArchive: true,
                    maxSize: '200m',
                    maxFiles: '90d'
                })],
    })
}

module.exports.init = function(name) {
    init(name);
};

module.exports.info = function(log) {
    logger.info(log);
}

module.exports.debug = function(log) {
    logger.debug(log);
}

module.exports.error = function(log) {
    logger.error(log);
}
