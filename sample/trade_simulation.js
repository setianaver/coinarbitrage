


var ex0 = [5000, 5];
var ex1 = [5000, 5];

function process(ticker) {

    var pct = ((ticker[1] - ticker[0]) / ticker[0]) * 100;
    console.log(pct + '%')
    if (Math.abs(pct) >= 5.0) {
        if (ex0[0] === 0 || ex1[1] === 0)
            return;

        if (ticker[1] > ticker[0]) {
            // ex1 sell
            ex1[0] = ex1[0] + ex1[1] * ticker[1]
            ex1[1] = 0;

            // ex0 buy
            ex0[1] = ex0[1] + (ex0[0] /ticker[0])
            ex0[0] = 0
        }
    } else if (Math.abs(pct) <= 1.0) {
        function clear(ex, tck) {
            if (ex[0] === 0 && ex[1] > 0.0 ) {
                // 코인 절반 판매
                var tmp = ex[1]/2;
                ex[0] = tmp * tck;
                ex[1] -= tmp
            } else if (ex[0] > 0 && ex[1] === 0.0 ) {
                // 코인 절반 매수
                var tmp = ex[0]/2;
                ex[1] = tmp / tck
                ex[0] -= tmp;
            }
        }

        clear(ex0, ticker[0])
        clear(ex1, ticker[1])
    }

    console.log(ex0)
    console.log(ex1)

    var tot = ex0[0] + ex0[1]*ticker[0]
     tot += ex1[0] + ex1[1]*ticker[1]
    console.log('자산가치 :' + tot)
}

console.log(ex0)
console.log(ex1)

process(  [1000, 1050])
process(  [1010, 1010])
process(  [900, 910])
process(  [910, 905])
process(  [1000, 1000])
process(  [1000, 1060])


