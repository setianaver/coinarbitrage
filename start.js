//import { createRequire } from 'module';
//const require = createRequire(import.meta.url);

const glob = require( 'glob' );
const path = require( 'path' );
const logger = require("./logger");

const trade = require( './trade' );

logger.init('cas');

global.logger = logger;
global.exchanges = {};
global.exchanges_arr = [];

function load_exchange(module_path) {
    return new Promise((resolve, reject) => {
        glob.sync(module_path).forEach((file) => {
            let fpath = path.resolve(file);

            try {
                let exch = path.basename(fpath, path.extname(fpath));
                exch = exch.toUpperCase();
                if (global.config[exch].use === false) {
                    console.log(exch + " not use...");
                    return
                }
                global.exchanges[exch] = require(fpath);
                global.exchanges[exch]["name"] = exch;
                global.exchanges_arr.push(exchanges[exch]);

                console.log(exch + " loaded...");
            } catch (e) {
                console.log('Error occured while loading ' + fpath + ' : ' + e);
            }
            resolve();
        });
    });
}

async function query_accounts() {
    for (const item of exchanges_arr) {
        var act = await item.get_accounts();
        logger.info("->" + item.name);
        logger.info(act);


    }
}

async function watch_tickers() {
    for (const item of exchanges_arr) {
        var act = await item.ticker();
        logger.info("->" + item.name);
        logger.info(act);
        exchanges_arr['ticker'] = act
    }
    //
    // var limits = exchanges_arr.reduce(function(limits, element, index, array){
    //     limits.max = element > array[limits.max] ? index : limits.max;
    //     limits.min = element < array[limits.min] ? index : limits.min;
    //     return limits;
    // }, { max : 0, min : 0 });
    return 0;
}

async function start_trade() {

    // 전체거래소, 코인 시세 조회
    await watch_tickers();

    // 매수, 매도 조건 체크
    // 매수, 매도 실행
    // 체결확인 필요하면 잔고조회 또는 미체결조회?

    setTimeout(start_trade, 10000);
}

async function start_server(_argv) {
    let argv = _argv.slice(1);
    if (argv.length === 2) {
        // Watch : node start.js [ConfType]
        // ex) node start.js dev
    } else {
        console.log('[Usage]');
        console.log('node start.js [Config Type]');
        console.log('ex) node start.js dev');
    }

    let conftype= argv[1];

    var config = require('./config');
    global.config = config.init(conftype);

    logger.init('cas_' + conftype);
    global.logger = logger;

    await load_exchange('./exchange/*.js');
    logger.info("Exchange module load complete... ");

    trade.start();
}

start_server(process.argv)