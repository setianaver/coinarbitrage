var config = require('../config');
global.config = config.init("dev");
var upbit = require('../exchange/upbit');

function calcOrderPrice(hoga_unit, price) {
    var i = 0;
    for (i = 0; i < hoga_unit.length; i++) {
        if (hoga_unit[i][0] > price) {
            break
        }
    }

    var adj_price = price - (price % hoga_unit[i][1])
    console.log(`${price} -> ${adj_price}`)
    return adj_price;
}


function find_hoga(hoga_unit, price) {
    var i = 0;
    for (i = 0; i < hoga_unit.length; i++) {
        if (hoga_unit[i][0] > price) {
            break
        }
    }

    // 현재 호가단위로 호가수만큼 이동
    return hoga_unit[i][1];
}

/*
    hoga_unit : 호가 유닛 [[가격, 단위], [가격, 단위]]
    price : 가격
    delta_hoga : 호가차이 정수
 */
function calc_order_price(hoga_unit, price, delta_hoga) {

    console.log(price + ", " + delta_hoga);

    // 현재 호가단위로 호가수만큼 이동
    var hoga1 = find_hoga(hoga_unit, price);
    price = price + (hoga1 * delta_hoga);

    // 수정된 값으로 호가단위 검색
    var hoga2 = find_hoga(hoga_unit, price);

    // 호가가 변했으면 호가 단위 맞춤
    if (hoga1 < hoga2) {
        if (hoga2 >= 1) {
            price = price - (price % hoga2);
        } else {
            // 분자가 소수점이면 나머지 연산자 동작안함
            var rem = price;
            while (rem > hoga2) {
                rem -= hoga2;
            }
            price = price - rem;
        }
    }

    console.log(price);
    return price;
}


async function test() {


    //var act = await upbit.order(param);
    //console.log(calcOrderPrice('upbit', 1000.001));
    // console.log(calcOrderPrice('upbit', 900.101));
    // console.log(calcOrderPrice('gopax', 900.601));
    //
    // console.log(calcOrderPrice('upbit', 1200.601));
    // console.log(calcOrderPrice('gopax', 1200.601));
    // //setTimeout(test, 1000);

    var hoga_unit = global.config.GOPAX.hoga_unit;
    console.log(calc_order_price(hoga_unit, 499, 12));
    //console.log(calcOrderPrice2('upbit', 998, 81));
}

setTimeout(test, 1000);

