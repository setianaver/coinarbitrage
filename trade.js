const db = require( './database/db' );

async function query_accounts() {
    for (const item of global.exchanges_arr) {
        var act = await item.get_accounts();
        logger.info("->" + item.name);
        logger.info(act);

        await db.reset_exch_account({exch: item.name})

        // save db
        for (const [key, value] of Object.entries(act)) {
            var param = {
                exch: item.name,
                market: key,
                avail: value.balance,
                locked: value.locked
            }

            item[key] = {avail: value.balance, locked: value.locked}

            await db.insert_account(param);
        }
    }
}


async function watch_tickers() {
    for (const item of global.exchanges_arr) {
        var act = await item.ticker();
        logger.info("->" + item.name);
        logger.info(act);
        item['price'] = act
    }

    return 0;
}

function check_minmax_price(coin) {
    var limits = global.exchanges_arr.reduce(function(limits, element, index, array){
        limits.max = element.price[coin] > array[limits.max].price[coin] ? index : limits.max;
        limits.min = element.price[coin] < array[limits.min].price[coin] ? index : limits.min;
        return limits;
    }, { max : 0, min : 0 });

    var maxEx = global.exchanges_arr[limits.max];
    var minEx = global.exchanges_arr[limits.min];

    logger.info(coin + " -------------------->" )
    logger.info("Max : " + maxEx.name + " " + maxEx.price[coin]);
    logger.info("Min : " + minEx.name + " " + minEx.price[coin]);
    var diff = (maxEx.price[coin] - minEx.price[coin]) / minEx.price[coin];
    logger.info("Diff : " + (diff*100).toFixed(1) + '%');
    logger.info("");
    return [maxEx, minEx, diff]
}

async function run_order() {
    for (const coin of global.config.COIN_LIST) {
        // [maxEx, minEx, diffRate]
        var cond = check_minmax_price(coin);

        // 매매조건에 부합되면 주문 : 5%이상 차이나면 고가매수, 저가매도?
        // 체결대기?
        if (cond[2] > 0.15) {
           // 매수
          // var price = 10000;
          // var volume = 1;
          // var param = {
          //       market: coin,
          //       side: 'buy',
          //       volume: volume,
          //       price: price,
          //       ord_type: 'limit'
          //   }
          //   var order_id = cond[0].order("buy")
          //   var order_info = {exch: cond[0].name, market: coin, price: price, volume: volume,
          //       remain_volume: volume, order_type: 'limit', status: 'wait', exch_order_id: order_id};
          //   db.insert_order(order_info);
          //
          //   cond[0].order_complete_test()
          //
          //   // 매도
          //   param = {
          //       market: coin,
          //       side: 'buy',
          //       volume: volume,
          //       price: price,
          //       ord_type: 'limit'
          //   }
          //   var order_id = cond[1].order(param)
          //   order_info = {exch: cond[1].name, market: coin, price: price, volume: volume,
          //       remain_volume: volume, order_type: 'limit', status: 'wait', exch_order_id: order_id};
          //   db.insert_order(order_info);
          //   cond[1].order_complete_test()
        }

    }
}

async function start_trade() {

    // 전체거래소, 코인 시세 조회
    await watch_tickers();

    await run_order();

    // 매수, 매도 조건 체크
    // 매수, 매도 실행
    // 체결확인 필요하면 잔고조회 또는 미체결조회?

    setTimeout(start_trade, 10000);
}

exports.start = async function () {

    await db.connect();

    await query_accounts();

    logger.info("Query account complete... ");

    start_trade();
}

